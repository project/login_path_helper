<?php
/**
 * @file
 * Contains Drupal\login_path_helper\Form|LoginPathHelperSettingsForm
 */

namespace Drupal\login_path_helper\Form;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Configure example settings for this site.
 */
class LoginPathHelperSettingsForm extends ConfigFormBase {

  /** 
   * Config settings.
   *
   * @var string
   */
  const SETTINGS = 'login_path_helper.settings';

  /** 
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'login_path_helper_admin_settings';
  }

  /** 
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      static::SETTINGS,
    ];
  }

  /** 
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config(static::SETTINGS);

    $form['login_path_helper_linkname'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Link Name'),
      '#description' => $this->t('This is the text that will be displayed as the login link. The default is "Site Login".'),
      '#default_value' => $config->get('login_path_helper_linkname'),
    ];  

    $form['login_path_helper_urlprefix'] = [
      '#type' => 'textfield',
      '#title' => $this->t('URL Prefix'),
      '#description' => $this->t('For normal user login, the default value "user/login" should be good. If the site uses SAML SSO, use "saml_login".'),
      '#default_value' => $config->get('login_path_helper_urlprefix'),
    ];  

    return parent::buildForm($form, $form_state);
  }

  /** 
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Retrieve the configuration.
    $this->configFactory->getEditable(static::SETTINGS)
      // Set the submitted configuration setting.
      ->set('login_path_helper_linkname', $form_state->getValue('login_path_helper_linkname'))
      // You can set multiple configurations at once by making
      // multiple calls to set().
      ->set('login_path_helper_urlprefix', $form_state->getValue('login_path_helper_urlprefix'))
      ->save();

    parent::submitForm($form, $form_state);
  }

}
