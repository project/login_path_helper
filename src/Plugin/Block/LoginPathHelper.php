<?php 

namespace Drupal\login_path_helper\Plugin\Block;
use Drupal\Core\Block\BlockBase;
use Drupal\Core\Url;

/**
 * Provides a 'Helper Login block that can be helpful for SSO redirection'
 * @Block(
 *  id = "login_path_helper",
 *  admin_label = @Translation("Login Path Helper"),
 *  category = @Translation("A Login Path Helper"),
 * )
 */

class LoginPathHelper extends BlockBase {

    /**
     * {@inheritdoc}
     */

   public function build () {
        $config = \Drupal::config('login_path_helper.settings');
        $linkname = $config->get('login_path_helper_linkname');
        $urlprefix = $config->get('login_path_helper_urlprefix');
        return [
        '#markup' => "<a href=\"https://".$this->buildLoginHost() . "/" . $urlprefix . $this->buildLoginPath() . "\">". $linkname . "</a>",
        ];
    }

     /**
     * {@inheritdoc}
     */

    public function getCacheMaxAge() {
        return 0;
    }

    private function buildLoginHost() {
        $current_host  = \Drupal::request()->getHost();
        return $current_host;
    }

    private function buildLoginPath() {
        $current_uri = \Drupal::request()->getRequestUri();
        return $current_uri;
    }

}
